//
//  VIFilterPreviewTableCell.h
//  VimgKeditor
//
//  Created by Илья on 24.09.12.
//  Copyright (c) 2012 ilya.sergeev@me.com. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "GPUImage.h"
#import "VIFiltersEnum.h"

@interface VIFilterPreviewTableCell : UITableViewCell

@property (unsafe_unretained, nonatomic) IBOutlet UIView *mainView;
@property (unsafe_unretained, nonatomic) IBOutlet GPUImageView *previewView;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *maskView;

@property (nonatomic) GPUImageOutput<GPUImageInput>* filter;
@property (strong, nonatomic) GPUImageOutput* imageOutput;
@property (nonatomic) BOOL didSelected;

- (void)turnFilter:(BOOL)onOrOff;

@end
