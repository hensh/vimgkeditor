//
//  VIFilterImagePreviewCell.h
//  VimgKeditor
//
//  Created by Илья on 24.09.12.
//  Copyright (c) 2012 ilya.sergeev@me.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VIFilterImagePreviewCell : UITableViewCell

@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *previewView;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *borderImageView;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *mainView;
@property (strong, nonatomic) UIImage* image;
@property (nonatomic) BOOL didSelected;

@end
