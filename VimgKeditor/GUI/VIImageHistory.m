//
//  VIImageHistory.m
//  VimgKeditor
//
//  Created by Илья on 24.09.12.
//  Copyright (c) 2012 ilya.sergeev@me.com. All rights reserved.
//

#import "VIImageHistory.h"

#import "VIStorage.h"
#import "VIAssetTableViewCell.h"
#import <AssetsLibrary/AssetsLibrary.h>

#define ImageBorderThickness 4

@implementation VIImageHistory
{
	NSArray* _assets;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
	_assets = [VIStorage imagesHistory];
	return _assets.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *identifier = @"VIAssetTableViewCell";
    
    VIAssetTableViewCell *_horizontalCell = (VIAssetTableViewCell*)[tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (!_horizontalCell)
	{
		CGAffineTransform rotateTable = CGAffineTransformMakeRotation(M_PI_2);
		NSArray* topLavelsObjs = [[NSBundle mainBundle] loadNibNamed:@"VIAssetTableViewCell" owner:nil options:nil];
		for (id obj in topLavelsObjs)
		{
			if ([obj isKindOfClass:[VIAssetTableViewCell class]])
			{
				_horizontalCell = obj;
				break;
			}
		}
		_horizontalCell.previewImage.transform = rotateTable;
    }
    
	NSString* urlString = _assets[indexPath.row];
	NSURL* url = [NSURL URLWithString:urlString];
    
    _horizontalCell.assetUrl = url;
    
    return _horizontalCell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
	return 88;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	NSString* urlString = _assets[indexPath.row];
	NSURL* url = [NSURL URLWithString:urlString];
	
	ALAssetsLibrary* assetLibrary = [ALAssetsLibrary new];
	[assetLibrary assetForURL:url
				  resultBlock:^(ALAsset *asset) {
					  if (asset != nil)
					  {
						  UIImage* representation = [UIImage imageWithCGImage:asset.defaultRepresentation.fullResolutionImage];
						  [self.delegate controller:nil
									   didPickImage:representation
										 withFilter:GPUFilterEmpty];
					  }
					  else
					  {
						  [VIStorage removeFromHistoryImageURL:url];
						  [tableView reloadData];
					  }
				  }
				 failureBlock:^(NSError *error) {
					 NSLog(@"can not load asset");
				 }];
}

@end
