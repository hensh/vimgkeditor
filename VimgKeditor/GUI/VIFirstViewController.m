//
//  VIViewController.m
//  VimgKeditor
//
//  Created by Илья on 20.09.12.
//  Copyright (c) 2012 ilya.sergeev. All rights reserved.
//

#import "VIFirstViewController.h"

#import <AssetsLibrary/AssetsLibrary.h>
#import "UIImage_resizable.h"
#import "VICameraRollViewController.h"
#import "VICameraPosibilieties.h"
#import "VIImageHistory.h"
#import "VIEditorViewController.h"
#import "VIStorage.h"
#import "VIImageSource.h"

#define MainTitleFont @"Lobster 1.4"

@interface VIFirstViewController () <UINavigationControllerDelegate, UIImagePickerControllerDelegate, VICameraRollViewControllerDelegate>

@property (unsafe_unretained, nonatomic) IBOutlet UIView *mainView;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *takePhotoButton;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *cameraRollButton;
@property (strong, nonatomic) IBOutlet VIImageHistory *imageHistory;
@property (unsafe_unretained, nonatomic) IBOutlet UITableView *imageTableView;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *mainTitle;

@end

@implementation VIFirstViewController
{
	BOOL isShowFirstTime;
}

- (void)viewDidUnload
{
	[self setMainView:nil];
	[self setTakePhotoButton:nil];
	[self setCameraRollButton:nil];
	[self setImageHistory:nil];
	[self setImageTableView:nil];

	[self setMainTitle:nil];
	[super viewDidUnload];
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	
	isShowFirstTime = YES;
	
	UIImage* saveBtnImage = [UIImage resizableImageWithName:@"SaveBtn"];
	[self.takePhotoButton setBackgroundImage:saveBtnImage forState:UIControlStateNormal];
	
	UIImage* saveBtn_PressedImage = [UIImage resizableImageWithName:@"SaveBtn_Pressed"];
	[self.takePhotoButton setBackgroundImage:saveBtn_PressedImage forState:UIControlStateHighlighted];
	
	self.cameraRollButton.hidden = !VICameraPosibilieties.hasCamera;
		
	UIImage* rollBtnImage = [UIImage resizableImageWithName:@"RollBtn"];
	[self.cameraRollButton setBackgroundImage:rollBtnImage forState:UIControlStateNormal];
	
	UIImage* rollBtn_PressedImage = [UIImage resizableImageWithName:@"RollBtn_Pressed"];
	[self.cameraRollButton setBackgroundImage:rollBtn_PressedImage forState:UIControlStateHighlighted];
	
	self.imageTableView.transform = CGAffineTransformMakeRotation(-M_PI_2);
	
	self.imageTableView.frame = CGRectMake(0, 400, 320, 80);
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
	return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	self.mainTitle.font = [UIFont fontWithName:MainTitleFont size:27];
	
	if (isShowFirstTime)
	{
		isShowFirstTime = NO;
		[self showMainView];
	}
	[self.imageTableView reloadData];
}

- (void)showMainView
{
	self.mainView.alpha = 0;
	[UIView animateWithDuration:1 animations:^{
		self.mainView.alpha = 1;
	}];
}

- (IBAction)takePhotoAction:(id)sender
{
	VICameraRollViewController* cameraRollController = [VICameraRollViewController sharedController];
	cameraRollController.delegate = self;
	[self.navigationController pushViewController:cameraRollController animated:YES];
}

- (IBAction)cameraRollAction:(id)sender
{
	UIImagePickerController* picker = [UIImagePickerController new];
	if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary])
	{
		picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
		picker.delegate = self;
		[self presentModalViewController:picker animated:YES];
	}
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
	[VIStorage saveToHistory:[info valueForKey:UIImagePickerControllerReferenceURL]];
	UIImage* image = [info valueForKey:UIImagePickerControllerOriginalImage];
	[self controller:picker didPickImage:image withFilter:GPUFilterEmpty];
	[picker dismissModalViewControllerAnimated:YES];
}

#pragma mark - VICameraRollViewControllerDelegate

- (void)controller:(UIViewController*)controller didPickImage:(UIImage*)image withFilter:(GPUFilter)filterNumber
{
	VIEditorViewController* _editorViewController = [VIEditorViewController new];
	CGRect testRect = _editorViewController.view.frame;
	testRect = testRect;
	_editorViewController.imageModel = [[VIImageSource alloc] initWithImage:image];
	_editorViewController.filterNumber = filterNumber;
	_editorViewController.canRetakeImage = [controller isKindOfClass:[VICameraRollViewController class]];
	[self.navigationController pushViewController:_editorViewController animated:YES];
}

@end
