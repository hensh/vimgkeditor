//
//  VIFiltersDataSource.m
//  VimgKeditor
//
//  Created by Илья on 24.09.12.
//  Copyright (c) 2012 ilya.sergeev@me.com. All rights reserved.
//

#import "VIFiltersDataSource.h"

#import "VICamera.h"
#import "VIFilterPreviewTableCell.h"
#import "VIFiltersFactory.h"

@implementation VIFiltersDataSource
{
	NSArray* _filters;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	if (tableView.hidden)
	{
		return 0;
	}
	
	NSMutableArray* filters = [NSMutableArray arrayWithCapacity:_filterNumbers.count];
	for (NSNumber* filterNumberObj in _filterNumbers)
	{
		GPUFilter filterNumber = [filterNumberObj intValue];
		[filters addObject:[VIFiltersFactory createFilterWithNumber:filterNumber]];
	}
	_filters = filters;
	
	return [super tableView:tableView numberOfRowsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	if (indexPath.section == ImagePreset)
	{
		return [super tableView:tableView cellForRowAtIndexPath:indexPath];
	}
	static NSString *identifier = @"VIFilterPreviewTableCell";
    
    VIFilterPreviewTableCell *cell = (VIFilterPreviewTableCell*)[tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (!cell)
	{
		CGAffineTransform rotateTable = CGAffineTransformMakeRotation(M_PI_2);
		NSArray* topLavelsObjs = [[NSBundle mainBundle] loadNibNamed:@"VIFilterPreviewTableCell" owner:nil options:nil];
		
		for (id obj in topLavelsObjs)
		{
			if ([obj isKindOfClass:[VIFilterPreviewTableCell class]])
			{
				cell = obj;
				break;
			}
		}
		cell.previewView.transform = rotateTable;
		cell.imageOutput = self.imageOutput;
    }
    
	cell.filter = _filters[indexPath.row];
	[cell turnFilter:YES];
	if ([_selectedFilterIndexPath isEqual:indexPath])
	{
		[tableView selectRowAtIndexPath:_selectedFilterIndexPath animated:YES scrollPosition:UITableViewScrollPositionNone];
	}
    return cell;
}

@end
