//
//  VIAssetTableViewCell.m
//  VimgKeditor
//
//  Created by Илья on 24.09.12.
//  Copyright (c) 2012 ilya.sergeev@me.com. All rights reserved.
//

#import "VIAssetTableViewCell.h"

#import <AssetsLibrary/AssetsLibrary.h>
#import "VIStorage.h"

@implementation VIAssetTableViewCell

+ (ALAssetsLibrary *)assetslibrary
{
	static ALAssetsLibrary* _assetslibrary = nil;
	static dispatch_once_t pred;
    
    if(!_assetslibrary)
    {
        dispatch_once(&pred, ^{
            _assetslibrary = [ALAssetsLibrary new];
        });
    }
    
    return _assetslibrary;
}

- (void)setAssetUrl:(NSURL *)assetUrl
{
	if ([assetUrl isEqual:_assetUrl])
	{
		return;
	}
	_assetUrl = assetUrl;
	[[VIAssetTableViewCell assetslibrary] assetForURL:assetUrl
										  resultBlock:^(ALAsset *asset) {
											  if (asset == nil)
											  {
												  //TODO загружать превьюху noImage
												  self.previewImage.image = nil;
												  [VIStorage removeFromHistoryImageURL:assetUrl];
												  return;
											  }
											  NSDictionary* assetURLDict = [asset valueForProperty:ALAssetPropertyURLs];
											  NSURL* assetOriginalURL = [[assetURLDict allValues] lastObject];
											  if ([assetOriginalURL isEqual:assetUrl])
											  {
												  self.previewImage.image = [UIImage imageWithCGImage:asset.thumbnail];
											  }
										  }
										 failureBlock:^(NSError *error) {
											 self.previewImage.image = nil;
											 [VIStorage removeFromHistoryImageURL:assetUrl];
											 NSLog(@"can not load image, error : %@", error);
										 }];
}


@end
