//
//  VIAssetTableViewCell.h
//  VimgKeditor
//
//  Created by Илья on 24.09.12.
//  Copyright (c) 2012 ilya.sergeev@me.com. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VIAssetTableViewCell : UITableViewCell

@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *previewImage;
@property (strong, nonatomic) NSURL* assetUrl;

@end
