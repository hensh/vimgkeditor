//
//  VIEditorViewController.m
//  VimgKeditor
//
//  Created by Ilya Sergeev on 26.09.12.
//  Copyright (c) 2012 ilya.sergeev@me.com. All rights reserved.
//

#import "VIEditorViewController.h"

#import "UIImage_resizable.h"
#import "VIFiltersDataSource.h"
#import "VIFiltersFactory.h"
#import "VIStorage.h"
#import "VIDeviceInfo.h"
#import "UIImage+TextDrawing.h"
#import "UIView+Move.h"

#define FilterViewFrame CGRectMake(0, 0, 320, 80)
#define DefaultImageYOffset 80
#define ImageScrollViewAdditionalDelta 70
#define FontPreviewSize 21
#define FontDefaultSize 21
#define FontDetailsSize 14
#define MaxTitleLenght 28
#define MaxDetailLenght 46

#define TitleTextDelta 12

@interface VIEditorViewController () <VIFiltersSelectionDelegate, UIScrollViewDelegate, UITextViewDelegate>


@property (unsafe_unretained, nonatomic) IBOutlet GPUImageView *imageView;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *saveButton;
@property (unsafe_unretained, nonatomic) IBOutlet UITableView *filtersTableView;
@property (strong, nonatomic) IBOutlet VIFiltersDataSource *imagePreviewDataSourece;
@property (unsafe_unretained, nonatomic) IBOutlet UIScrollView *imageScrollView;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *progressView;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *inputTextViewBackground;
@property (unsafe_unretained, nonatomic) IBOutlet UITextView *inputTextView;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *fontSelectionView;
@property (strong, nonatomic) IBOutletCollection(UIButton) NSArray* fontsButtons;
@property (unsafe_unretained, nonatomic) IBOutlet UIView *bottonBarView;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *retakeButton;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *textLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *detailsLabel;
@property (unsafe_unretained, nonatomic) IBOutlet UIImageView *presetImageView;
@property (unsafe_unretained, nonatomic) IBOutlet UIButton *captionButton;

@end

@implementation VIEditorViewController
{
	GPUImageOutput<GPUImageInput>* _filter;
	VIPreset* _preset;
}

+ (NSArray*)fonts
{
	static NSArray* fontNames = nil;
    static dispatch_once_t pred;
	
	dispatch_once(&pred, ^{
		fontNames = @[@"UK_Arbat", @"a_Gildia", @"AGReverance", @"Cheshirskiy Cat", @"Avalon Medium", @"Helvetica"];
	});
	
	return fontNames;
}

- (void) prepareFontButtons
{
	NSArray* fonts = [VIEditorViewController fonts];
	
	for (UIButton* button in self.fontsButtons)
	{
		button.titleLabel.font = [UIFont fontWithName:fonts[button.tag] size:FontPreviewSize];
	}
}

- (void)setFilterNumber:(GPUFilter)filterNumber
{
	[_filter removeTarget:self.imageView];
	if (_filter != nil)
	{
		[_imageModel.gpuSource removeTarget:_filter];
	}
	
	_filterNumber = filterNumber;
	_filter = [VIFiltersFactory createFilterWithNumber:filterNumber];
	
	[_filter forceProcessingAtSize:self.imageView.sizeInPixels];
	if (self.imageView != nil)
	{
		[_filter addTarget:self.imageView];
		[_imageModel.gpuSource addTarget:_filter];
		
		[_imageModel.gpuSource processImage];
	}
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	
	self.inputTextViewBackground.image = [UIImage resizableImageWithName:@"Input"];
	_preset = [VIPreset avalablePresets][0];
	
	UIImage* saveBtnImage = [UIImage resizableImageWithName:@"SaveBtn"];
	[self.saveButton setBackgroundImage:saveBtnImage forState:UIControlStateNormal];
	
	UIImage* saveBtn_PressedImage = [UIImage resizableImageWithName:@"SaveBtn_Pressed"];
	[self.saveButton setBackgroundImage:saveBtn_PressedImage forState:UIControlStateHighlighted];
	
	self.filtersTableView.transform = CGAffineTransformMakeRotation(-M_PI_2);
	self.filtersTableView.frame = FilterViewFrame;
	
	self.imageView.fillMode = kGPUImageFillModePreserveAspectRatioAndFill;
	self.imageScrollView.zoomScale = 1;
	
	[_filter addTarget:self.imageView];
	[_imageModel.gpuSource addTarget:_filter];
	self.imagePreviewDataSourece.showPresets = YES;
}

- (void)prepareRetakeButtonTitle
{
	NSString* retakeButtonTitle = nil;
	if (self.canRetakeImage)
	{
		retakeButtonTitle = NSLocalizedString(@"Retake", nil);
	}
	else
	{
		retakeButtonTitle = NSLocalizedString(@"Cancel", nil);
	}
	self.retakeButton.titleLabel.text = retakeButtonTitle;
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	[self prepareFontButtons];
	
	self.progressView.hidden = YES;
	
	NSString* fontFamily = [VIEditorViewController fonts][0];
	self.textLabel.font = [UIFont fontWithName:fontFamily size:FontDefaultSize];
	self.detailsLabel.font = [UIFont fontWithName:fontFamily size:FontDetailsSize];
	self.inputTextView.text = @"";
	
	[self prepareRetakeButtonTitle];
	
	self.fontSelectionView.alpha = 0;
	
	self.imagePreviewDataSourece.selectedFilterNumber = _filterNumber;
	[_imageModel.gpuSource processImage];
		
	//TODO нужен рефакторинг!!!
	CGRect imageViewFrame = self.imageView.frame;
	CGSize imageScrollViewSize = _imageScrollView.frame.size;
	CGSize previewSize = _imageModel.preview.size;
	
	float scaleFactor = MIN(previewSize.height/imageScrollViewSize.height, previewSize.width/imageScrollViewSize.width);
	imageViewFrame.size = CGSizeMake(previewSize.width / scaleFactor, previewSize.height / scaleFactor);
	_imageView.frame = imageViewFrame;
	_imageScrollView.contentSize = imageViewFrame.size;
	
	[_filter forceProcessingAtSize:self.imageView.sizeInPixels];

	CGFloat offset = 0;
	if (_imageScrollView.contentSize.height > DefaultImageYOffset + imageScrollViewSize.height)
	{
		offset = DefaultImageYOffset;
	}
	_imageScrollView.contentOffset = CGPointMake(0, offset);
	
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(liftToolbarWhenKeybordAppears:)
												 name:UIKeyboardWillShowNotification
											   object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(returnToolbarToInitialPosition:)
												 name:UIKeyboardWillHideNotification
											   object:nil];
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	
	[_filter removeAllTargets];
	[_imageModel.gpuSource removeAllTargets];
	_imageModel = nil;
	
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
	[[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewDidUnload
{
	[self setImageView:nil];
	[self setSaveButton:nil];
	[self setFiltersTableView:nil];
	[self setImagePreviewDataSourece:nil];
	[self setImageScrollView:nil];
	[self setImageView:nil];
	[self setProgressView:nil];
	[self setInputTextView:nil];
	[self setFontSelectionView:nil];
	[self setFontsButtons:nil];
	[self setInputTextViewBackground:nil];
	[self setBottonBarView:nil];
	[self setRetakeButton:nil];
	[self setTextLabel:nil];
	
	[self setPresetImageView:nil];
	[self setDetailsLabel:nil];
	[self setCaptionButton:nil];
	[self setSaveButton:nil];
	[super viewDidUnload];
}

- (IBAction)retakeAction:(id)sender
{
	[self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)saveAction:(id)sender
{	
	self.progressView.hidden = NO;

	//TODO сделать жесткий рефакторинг!!!
	float xScaleFactor = _imageModel.image.size.width / self.imageScrollView.contentSize.width;
	float yScaleFactor = _imageModel.image.size.height / self.imageScrollView.contentSize.height;
	
	CGRect visibleRect = [self.imageScrollView convertRect:self.imageScrollView.bounds toView:self.imageView];
	visibleRect.origin.x *= xScaleFactor;
	visibleRect.origin.y *= yScaleFactor;
	visibleRect.size.height *= yScaleFactor;
	visibleRect.size.width *= xScaleFactor;
	
	GPUImageOutput<GPUImageInput>* workingFilter = [VIFiltersFactory createFilterWithNumber:_filterNumber];
	
	UIImage* result = [workingFilter imageByFilteringImage:_imageModel.image];
	result = [result cropWithRect:visibleRect];
	
	result = [result imageWithMaskImage:self.presetImageView.image];
	result = [self drawInImage:result
				 textFromLabel:self.textLabel
				  scaleFactorX:xScaleFactor
				  scaleFactorY:yScaleFactor];
	
	result = [self drawInImage:result
				 textFromLabel:self.detailsLabel
				  scaleFactorX:xScaleFactor
				  scaleFactorY:yScaleFactor];
	
	[VIStorage saveImage:result
		 completionBlock:^{
			 [self.navigationController popToRootViewControllerAnimated:YES];
		 }
			  errorBlock:^(NSError *error) {
				  NSLog(@"can not save image");
				  self.progressView.hidden = YES;
				  [[[UIAlertView alloc] initWithTitle:@"Error"
											 message:@"Can not save image"
											delegate:nil
								   cancelButtonTitle:@"OK"
									otherButtonTitles:nil] show];
			  }];
}

- (UIImage*)drawInImage:(UIImage*)image
		  textFromLabel:(UILabel*)label
		   scaleFactorX:(float)xScaleFactor
		   scaleFactorY:(float)yScaleFactor
{
	CGRect textRect = [self.view convertRect:label.frame toView:self.presetImageView];
//	textRect.origin.x -= self.imageScrollView.contentOffset.x;
//	textRect.origin.y -= self.imageScrollView.contentOffset.y;
	textRect.origin.y += 15;
	textRect.origin.x *= xScaleFactor;
	textRect.origin.y *= yScaleFactor;
	textRect.size.height *= yScaleFactor;
	textRect.size.width *= xScaleFactor;
	
	UIFont* currentFont = label.font;
	int fontSize = label.font.pointSize * MIN(xScaleFactor, yScaleFactor);
	UIFont* drawingFont = [UIFont fontWithName:currentFont.familyName size:fontSize];
	
	return [image imageWithText:label.text
					   withFont:drawingFont
						  color:label.textColor
						 inRect:textRect
					  alignment:NSTextAlignmentCenter];
}

- (IBAction)selectFontAction:(UIButton*)senderButton
{
	for (UIButton* button in self.fontsButtons)
	{
		button.selected = NO;
	}
	senderButton.selected = YES;
	
	NSString* fontFamily = [VIEditorViewController fonts][senderButton.tag];
	self.textLabel.font = [UIFont fontWithName:fontFamily size:FontDefaultSize];
	self.detailsLabel.font = [UIFont fontWithName:fontFamily size:FontDetailsSize];
}

- (IBAction)captureShowAction:(UIButton*)senderButton
{
	self.captionButton.selected = !self.captionButton.selected;
	if (senderButton.selected)
	{
		[self.inputTextView becomeFirstResponder];
	}
	else
	{
		[self.inputTextView resignFirstResponder];
	}
	[UIView animateWithDuration:0.2 animations:^{
		CGFloat alpha = self.captionButton.selected?0:1;
		self.retakeButton.alpha = alpha;
		self.saveButton.alpha = alpha;
	}];
}

#pragma mark - VIFiltersSelectionDelegate

- (void)filtersDataSource:(id)dataSource didSelectFilter:(GPUFilter)filterNumber
{
	self.filterNumber = filterNumber;
}

- (void)filtersDataSource:(id)dataSource didSelectPreset:(VIPreset*)preset
{
	_preset = preset;
	self.presetImageView.image = preset.image;
	self.textLabel.textColor = preset.fontColor;
	self.detailsLabel.textColor = preset.fontColor;
}

#pragma mark - UIScrollViewDelegate

- (UIView*)viewForZoomingInScrollView:(UIScrollView *)aScrollView
{
    return self.imageView;
}

#pragma mark - UITextViewDelegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
	NSString* resultText = [textView.text stringByReplacingCharactersInRange:range withString:text];
	NSArray* lines = [resultText componentsSeparatedByString:@"\n"];
	
	if (lines.count > 2) return NO;
	NSString* line0 = (lines.count > 0)?lines[0]:nil;
	NSString* line1 = (lines.count > 1)?lines[1]:nil;

	if (line0.length == MaxTitleLenght && line1 == nil)
	{
		textView.text = [NSString stringWithFormat:@"%@\n", line0];
		return YES;
	}
	
	return line0.length <= MaxTitleLenght && line1.length <= MaxDetailLenght;
}

- (void)textViewDidChange:(UITextView *)textView
{
	NSArray* lines = [textView.text componentsSeparatedByString:@"\n"];
	if (lines.count == 1)
	{
		self.textLabel.text = lines[0];
		if (!self.detailsLabel.hidden)
		{
			self.detailsLabel.hidden = YES;
			
			[UIView animateWithDuration:0.2 animations:^{
				[self.textLabel moveWithDeltaX:0 deltaY:TitleTextDelta];
			}];
		}
	}
	else
	{
		self.textLabel.text = lines[0];
		self.detailsLabel.text = lines[1];
	
		if (self.detailsLabel.hidden)
		{
			[UIView animateWithDuration:0.2 animations:^{
				[self.textLabel moveWithDeltaX:0 deltaY:-TitleTextDelta];
				self.detailsLabel.hidden = NO;
			}];
		}
	}
	
}

#pragma mark - Moving toolbar with keyboard

- (void)moveToolbarUp:(BOOL)upOrDown notification:(NSNotification*)aNotification
{
	self.imageScrollView.scrollEnabled = !upOrDown;
	
	NSDictionary* userInfo = [aNotification userInfo];
	UIViewAnimationCurve animationCurve;
	NSTimeInterval animationDuration;
	[[userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey] getValue:&animationCurve];
	[[userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] getValue:&animationDuration];
	
	CGRect keyboardEndFrame;
	[[userInfo objectForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardEndFrame];
	CGRect keyboardFrame = [self.view convertRect:keyboardEndFrame toView:nil];
	
	[UIView animateWithDuration:animationDuration
					 animations:^{
						 [UIView setAnimationCurve:animationCurve];
						 
						 self.fontSelectionView.alpha = upOrDown?1:0;
						 
						 CGFloat yDelta = keyboardFrame.size.height * (upOrDown ? -1.f : +1.f);
						 
						 [self.bottonBarView moveWithDeltaX:0 deltaY:yDelta];
						 [self.fontSelectionView moveWithDeltaX:0 deltaY:yDelta];
						 
						 CGFloat additionalYDelta = yDelta + (upOrDown?-ImageScrollViewAdditionalDelta:ImageScrollViewAdditionalDelta);
						 [self.imageScrollView moveWithDeltaX:0 deltaY:additionalYDelta];
						 [self.textLabel moveWithDeltaX:0 deltaY:additionalYDelta];
						 [self.detailsLabel moveWithDeltaX:0 deltaY:additionalYDelta];
						 [self.presetImageView moveWithDeltaX:0 deltaY:additionalYDelta];
					 }];
}

- (void) liftToolbarWhenKeybordAppears:(NSNotification*)aNotification
{
	[self moveToolbarUp:YES notification:aNotification];
}

- (void) returnToolbarToInitialPosition:(NSNotification*)aNotification
{
	[self moveToolbarUp:NO notification:aNotification];
}

@end
