//
//  VIEditorViewController.h
//  VimgKeditor
//
//  Created by Ilya Sergeev on 26.09.12.
//  Copyright (c) 2012 ilya.sergeev@me.com. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "VIFiltersEnum.h"
#import "VIImageSource.h"

@interface VIEditorViewController : UIViewController

@property BOOL canRetakeImage;
@property (nonatomic) GPUFilter filterNumber;
@property (strong) VIImageSource* imageModel;

@end
