//
//  VIFilterImagePreviewDataSource.h
//  VimgKeditor
//
//  Created by Илья on 24.09.12.
//  Copyright (c) 2012 ilya.sergeev@me.com. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "VIFilterPreviewDataSource.h"

@interface VIFilterImagePreviewDataSource : VIFilterPreviewDataSource

@end
