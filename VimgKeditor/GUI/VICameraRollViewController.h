//
//  VICameraRollViewController.h
//  VimgKeditor
//
//  Created by Илья on 21.09.12.
//  Copyright (c) 2012 ilya.sergeev@me.com. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "VICameraRollViewControllerDelegate.h"

@interface VICameraRollViewController : UIViewController

@property (unsafe_unretained) id<VICameraRollViewControllerDelegate> delegate;

+ (VICameraRollViewController*)sharedController;

@end