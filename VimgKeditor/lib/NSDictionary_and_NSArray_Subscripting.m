//
//  NSObject+NSDictionary_and_NSArray_Subscripting.m
//  iZasada
//
//  Created by Alex D Vallianos on 08.08.12.
//  Copyright (c) 2012 VenturezLab. All rights reserved.
//

#import "NSDictionary_and_NSArray_Subscripting.h"

#if __IPHONE_OS_VERSION_MIN_REQUIRED <= __IPHONE_5_1

@implementation NSArray (SubscriptingFromIOS6)
- (id)objectAtIndexedSubscript:(NSUInteger)index
{
    return [self objectAtIndex:index];
}
@end

@implementation NSMutableArray (SubscriptingFromIOS6)
- (void)setObject:(id)object atIndexedSubscript:(NSUInteger)index
{
    [self replaceObjectAtIndex:index withObject:object];
}
@end

@implementation NSDictionary (SubscriptingFromIOS6)
- (id)objectForKeyedSubscript:(id)key
{
    return [self objectForKey:key];
}
@end

@implementation NSMutableDictionary (SubscriptingFromIOS6)
- (void)setObject:(id)object forKeyedSubscript:(id<NSCopying>)key
{
    [self setObject:object forKey:key];
}
@end

@implementation NSUserDefaults (SubscriptingFromIOS6)
- (id)objectForKeyedSubscript:(NSString *)key
{
    return [self objectForKey:key];
}

- (void)setObject:(id)object forKeyedSubscript:(NSString *)key
{
    [self setObject:object forKey:key];
}
@end

#endif