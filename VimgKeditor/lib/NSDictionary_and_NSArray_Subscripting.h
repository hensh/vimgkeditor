//
//  NSObject+NSDictionary_and_NSArray_Subscripting.h
//  iZasada
//
//  Created by Alex D Vallianos on 08.08.12.
//  Copyright (c) 2012 VenturezLab. All rights reserved.
//

#import <Foundation/Foundation.h>

#if __IPHONE_OS_VERSION_MIN_REQUIRED <= __IPHONE_5_1

@interface NSArray (SubscriptingFromIOS6)
- (id)objectAtIndexedSubscript:(NSUInteger)index;
@end

@interface NSMutableArray (SubscriptingFromIOS6)
- (void)setObject:(id)object atIndexedSubscript:(NSUInteger)index;
@end

@interface NSDictionary (SubscriptingFromIOS6)
- (id)objectForKeyedSubscript:(id)key;
@end

@interface NSMutableDictionary (SubscriptingFromIOS6)
- (void)setObject:(id)object forKeyedSubscript:(id<NSCopying>)key;
@end

@interface NSUserDefaults (SubscriptingFromIOS6)
- (id)objectForKeyedSubscript:(NSString *)key;
- (void)setObject:(id)object forKeyedSubscript:(NSString *)key;
@end

#endif