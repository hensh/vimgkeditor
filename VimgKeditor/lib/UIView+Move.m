//
//  UIView+Move.m
//  VimgKeditor
//
//  Created by Ilya Sergeev on 01.10.12.
//  Copyright (c) 2012 ilya.sergeev@me.com. All rights reserved.
//

#import "UIView+Move.h"

@implementation UIView(Move)

- (void)moveWithDeltaX:(CGFloat)dx deltaY:(CGFloat)dy
{
	CGRect frame = self.frame;
	frame.origin.y += dy;
	frame.origin.x += dx;
	self.frame = frame;
}

@end
