//
//  UIImage_resizable.h
//  VimgKeditor
//
//  Created by Илья on 21.09.12.
//  Copyright (c) 2012 ilya.sergeev. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIImage(resizable)

+ (UIImage*)resizableImageWithName:(NSString*)name;
- (UIImage*)imageScaledToSize:(CGSize)preferSize;

@end
