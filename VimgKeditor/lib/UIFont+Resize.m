//
//  UIFont+Resize.m
//  VimgKeditor
//
//  Created by Ilya Sergeev on 01.10.12.
//  Copyright (c) 2012 ilya.sergeev@me.com. All rights reserved.
//

#import "UIFont+Resize.h"

@implementation UIFont(Resize)

- (UIFont*)scaleWithFactor:(float)scaleFactor
{
	return [UIFont fontWithName:self.familyName
						   size:self.pointSize * scaleFactor];
}

@end
