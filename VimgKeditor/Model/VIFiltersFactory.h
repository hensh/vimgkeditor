//
//  VIFiltersFactory.h
//  VimgKeditor
//
//  Created by Илья on 24.09.12.
//  Copyright (c) 2012 ilya.sergeev@me.com. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "GPUImage.h"
#import "VIFiltersEnum.h"

typedef void (^VICompleteHandler)();

@interface VIFiltersFactory : NSObject

+ (void)prepareFiltersPreviewCompleteBlock:(VICompleteHandler)completeBlock;
+ (UIImage*)previewImageForFilter:(GPUFilter)filterNumber;

+ (VIFiltersFactory*)previewFiltersFactory;
+ (VIFiltersFactory*)fullScreenFiltersFactory;
+ (GPUImageOutput <GPUImageInput>*)createFilterWithNumber:(GPUFilter)filterNumber;
+ (GPUImageOutput <GPUImageInput>*)createCropFilter:(CGRect)cropRect;

+ (NSArray*)avalableFilters;

@end
