//
//  VIDeviceInfo.m
//  VimgKeditor
//
//  Created by Илья on 29.09.12.
//  Copyright (c) 2012 ilya.sergeev@me.com. All rights reserved.
//

#import "VIDeviceInfo.h"

#import <sys/utsname.h>

@implementation VIDeviceInfo

+ (NSString*)deviceVersion
{
	static NSString* deviceVersion = nil;
    static dispatch_once_t pred;
    
	dispatch_once(&pred, ^{
		struct utsname systemInfo;
		uname(&systemInfo);
		deviceVersion = [NSString stringWithCString:systemInfo.machine encoding:NSUTF8StringEncoding];
	});
	return deviceVersion;
}

+ (BOOL)isLowForceDevice
{
	static BOOL isForceDevice = NO;
    static dispatch_once_t pred;
    
	dispatch_once(&pred, ^{
		
		NSString* deviceVersion = [self deviceVersion];
		NSArray* unavalableDevices = @[@"iPhone1", @"iPhone2", @"iPhone3", @"iPod1", @"iPod2"];
		
		for (NSString* unavalableDevice in unavalableDevices)
		{
			if ([deviceVersion rangeOfString:unavalableDevice].length > 0)
			{
				isForceDevice = YES;
				break;
			}
		}
	});
    
    return isForceDevice;
}

+ (CGSize)photoSizeForDevicePosition:(AVCaptureDevicePosition)position
{
	NSString* deviceVersion = [self deviceVersion];
	
	if ([deviceVersion isEqualToString:@"iPhone2,1"]) //iPhone 3GS
	{
		return CGSizeMake(2048, 1536);
	}
	else
	{
		if (position == AVCaptureDevicePositionBack)
		{
			return CGSizeMake(2592, 1936);
		}
		else
		{
			return CGSizeMake(640, 480);
		}
	}
}

@end
