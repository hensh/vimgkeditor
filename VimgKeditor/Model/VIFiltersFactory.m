//
//  VIFiltersFactory.m
//  VimgKeditor
//
//  Created by Илья on 24.09.12.
//  Copyright (c) 2012 ilya.sergeev@me.com. All rights reserved.
//

#import "VIFiltersFactory.h"

#define BasicImageName @"BasicImage"
typedef GPUImageFilter* (^VIFilterCreatorHandler)();

@implementation VIFiltersFactory

+ (NSMutableDictionary*)imagePreviews
{
	static NSMutableDictionary *_imagePreview = nil;
    static dispatch_once_t pred;
	
	if(!_imagePreview)
    {
        dispatch_once(&pred, ^{
			_imagePreview = [NSMutableDictionary new];
		});
	}
	return _imagePreview;
}

+ (void)prepareFiltersPreviewCompleteBlock:(VICompleteHandler)completeBlock
{
	NSMutableDictionary* imagePreviews = [self imagePreviews];
	NSArray* avalableFilters = self.avalableFilters;
	
	for (NSNumber* filterNumber in avalableFilters)
	{
		GPUFilter filterNumberInt = [filterNumber intValue];
		GPUImageOutput<GPUImageInput>* filter = [VIFiltersFactory createFilterWithNumber:filterNumberInt];
		UIImage* filteredImage = nil;
		NSString* directoryPath = NSTemporaryDirectory();
		NSString* filteName = [NSString stringWithFormat:@"%@.png", [filter class]];
		NSString* filePath = [directoryPath stringByAppendingPathComponent:filteName];
		if ([[NSFileManager defaultManager] fileExistsAtPath:filePath])
		{
			filteredImage = [UIImage imageWithContentsOfFile:filePath];
		}
		else
		{
			UIImage* originImage = [UIImage imageNamed:BasicImageName];
			filteredImage = [filter imageByFilteringImage:originImage];
			NSData* imageData = UIImagePNGRepresentation(filteredImage);
			[imageData writeToFile:[directoryPath stringByAppendingPathComponent:filteName] atomically:YES];
		}
		[imagePreviews setValue:filteredImage forKey:[NSString stringWithFormat:@"%d", filterNumberInt]];
	}
}

+ (UIImage*)previewImageForFilter:(GPUFilter)filterNumber
{
	return [[self imagePreviews] valueForKey:[NSString stringWithFormat:@"%d", filterNumber]];
}

+ (VIFiltersFactory*)previewFiltersFactory
{
	static VIFiltersFactory *previewFiltersFactory = nil;
    static dispatch_once_t pred;
	
	if(!previewFiltersFactory)
    {
        dispatch_once(&pred, ^{
			previewFiltersFactory = [VIFiltersFactory new];
		});
	}
	return previewFiltersFactory;
}

+ (VIFiltersFactory*)fullScreenFiltersFactory
{
	static VIFiltersFactory *fullScreenFiltersFactory = nil;
    static dispatch_once_t pred;
	
	if(!fullScreenFiltersFactory)
    {
        dispatch_once(&pred, ^{
			fullScreenFiltersFactory = [VIFiltersFactory new];
		});
	}
	return fullScreenFiltersFactory;
}

+ (NSArray*)avalableFilters
{
	static NSArray *_filters = nil;
    static dispatch_once_t pred;
	
	if(!_filters)
    {
        dispatch_once(&pred, ^{
			_filters = @[	@(GPUFilterEmpty),
							@(GPUFilterSepia),
							@(GPUFilterColorInvert),
							@(GPUFilterGrayscale),
							@(GPUFilterSobelEdgeDetection),
							@(GPUFilterFalseColor),
							@(GPUFilterAmatorkaFilter),
							@(GPUFilterMissEtikate),
							@(GPUFilterSoftElegance),
							@(GPUFilterToon),
							@(GPUFilterEmboss),
							@(GPUFilterPinchDistortion)
				];
		});
	}
	return _filters;
}

+ (GPUImageOutput <GPUImageInput>*)createFilterWithNumber:(GPUFilter)filterNumber
{
	switch (filterNumber)
	{
		case GPUFilterEmpty:
			return [GPUImageFilter new];
			
		case GPUFilterSepia:
			return [GPUImageSepiaFilter new];
			
		case GPUFilterColorInvert:
			return [GPUImageColorInvertFilter new];
		
		case GPUFilterGrayscale:
			return [GPUImageGrayscaleFilter new];
			
		case GPUFilterSobelEdgeDetection:
			return [GPUImageSobelEdgeDetectionFilter new];
			
		case GPUFilterFalseColor:
			return [GPUImageFalseColorFilter new];
			
		case GPUFilterAmatorkaFilter:
			return [GPUImageAmatorkaFilter new];
			
		case GPUFilterMissEtikate:
			return [GPUImageMissEtikateFilter new];
			
		case GPUFilterToon:
			return [GPUImageSmoothToonFilter new];
			
		case GPUFilterEmboss:
			return [GPUImageEmbossFilter new];
			
		case GPUFilterPinchDistortion:
			return [GPUImagePinchDistortionFilter new];
		
		case GPUFilterGausianBlur:
			return [GPUImageGaussianBlurFilter new];
			
		case GPUFilterSoftElegance:
			return [GPUImageSoftEleganceFilter new];
			
		default:
			return nil;
	}
	return nil;
}

+ (GPUImageOutput <GPUImageInput>*)createCropFilter:(CGRect)cropRect
{
	return [[GPUImageCropFilter alloc] initWithCropRegion:cropRect];
}

@end
