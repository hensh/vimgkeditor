//
//  VIStorage.m
//  VimgKeditor
//
//  Created by Илья on 21.09.12.
//  Copyright (c) 2012 ilya.sergeev. All rights reserved.
//

#import "VIStorage.h"

#import "VIImageSaver.h"
#import <AssetsLibrary/AssetsLibrary.h>

#define ImageHistoryKey @"ImageHistoryKey"
#define ImageInHistoryCount 20

@implementation VIStorage

+ (NSMutableArray*)saversArray
{
	static NSMutableArray *_saversArray = nil;
    static dispatch_once_t pred;
    
    if(!_saversArray)
    {
        dispatch_once(&pred, ^{
            _saversArray = [NSMutableArray new];
        });
    }
    
    return _saversArray;
}

+ (NSArray*)imagesHistory
{
	NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
	return [userDefaults valueForKey:ImageHistoryKey];
}

+ (void)saveImage:(UIImage*)image
  completionBlock:(VICompletionHandler)completionBlock
	   errorBlock:(VIErrorHandler)errorBlock
{
	__block VIImageSaver* _saver = [VIImageSaver saveImage:image
										   completionBlock:^(NSURL *url) {
											   [[VIStorage saversArray] removeObject:_saver];
											   [self saveToHistory:url];
											   completionBlock();
										   } errorBlock:^(NSError *error) {
											   [[VIStorage saversArray] removeObject:_saver];
											   errorBlock(error);
										   }];
	[[VIStorage saversArray] addObject:_saver];
}

+ (void)saveToHistory:(NSURL*)imageUrl
{
	NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
	NSMutableArray* history = [[userDefaults valueForKey:ImageHistoryKey] mutableCopy];
	if (history == nil)
	{
		history = [NSMutableArray new];
	}
	NSString* urlString = [imageUrl absoluteString];
	[history removeObject:urlString];
	[history insertObject:urlString atIndex:0];
	if (history.count > ImageInHistoryCount)
	{
		NSRange range = NSMakeRange(ImageInHistoryCount, history.count - ImageInHistoryCount);
		[history removeObjectsInRange:range];
	}
	
	[userDefaults setValue:history forKey:ImageHistoryKey];
	if (![userDefaults synchronize])
	{
		NSLog(@"can not synchronize");
	}
}

+ (void)removeFromHistoryImageURL:(NSURL*)imageURL
{
	NSUserDefaults* userDefaults = [NSUserDefaults standardUserDefaults];
	NSMutableArray* history = [[userDefaults valueForKey:ImageHistoryKey] mutableCopy];
	NSString* absoluteString = imageURL.absoluteString;
	for (int i = 0; i < history.count; i++)
	{
		if ([history[i] isEqualToString:absoluteString])
		{
			[history removeObject:absoluteString];
			break;
		}
	}
	[history removeObject:imageURL];
	[userDefaults setValue:history forKey:ImageHistoryKey];
	[userDefaults synchronize];
}

@end
