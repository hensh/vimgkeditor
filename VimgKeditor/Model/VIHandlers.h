//
//  VIHandlers.h
//  VimgKeditor
//
//  Created by Илья on 21.09.12.
//  Copyright (c) 2012 ilya.sergeev. All rights reserved.
//

#ifndef VimgKeditor_VIHandlers_h
#define VimgKeditor_VIHandlers_h

typedef void (^VIErrorHandler)(NSError* error);
typedef void (^VICompletionHandler)();
typedef void (^VICompletionHandlerWithURL)(NSURL*);

#endif
