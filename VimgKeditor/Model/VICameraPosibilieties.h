//
//  VICameraPosibilieties.h
//  VimgKeditor
//
//  Created by Илья on 21.09.12.
//  Copyright (c) 2012 ilya.sergeev@me.com. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <AVFoundation/AVFoundation.h>

@interface VICameraPosibilieties : NSObject

+ (BOOL)hasCamera;
+ (BOOL)hasFewCameras;

+ (NSArray*)flashModesForDevice:(AVCaptureDevice*)device;
+ (NSString*)stringOfFlashMode:(AVCaptureFlashMode)flashMode;

@end
