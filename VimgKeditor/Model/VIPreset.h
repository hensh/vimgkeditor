//
//  VIPresetsFactory.h
//  VimgKeditor
//
//  Created by Илья on 01.10.12.
//  Copyright (c) 2012 ilya.sergeev@me.com. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VIPreset : NSObject

+ (NSArray*)avalablePresets;

@property (readonly) UIImage* image;
@property (readonly) UIColor* fontColor;

+ (VIPreset*)presetWithImage:(UIImage*)image fontColor:(UIColor*)fontColor;
- (id)initWithImage:(UIImage*)image fontColor:(UIColor*)fontColor;

@end
