//
//  VICamera.m
//  VimgKeditor
//
//  Created by Илья on 24.09.12.
//  Copyright (c) 2012 ilya.sergeev@me.com. All rights reserved.
//

#import "VICamera.h"

#define BasicImageName @"BasicImage"

@implementation VICamera

+ (VICamera*)sharedCamera
{
	static VICamera* camera = nil;
	static dispatch_once_t pred;
    
    if(!camera)
    {
        dispatch_once(&pred, ^{
            camera = [VICamera new];
        });
    }
    
    return camera;
}

- (id)init
{
	self = [super init];
	if (self != nil)
	{
		_stillCamera = [GPUImageStillCamera new];
		_stillCamera.outputImageOrientation = UIInterfaceOrientationPortrait;
	}
	return self;
}

- (AVCaptureDevice*)captureDevice
{
	return _stillCamera.inputCamera;
}

- (void)startCapture
{
	[_stillCamera startCameraCapture];
}

- (void)stopCapture
{
	[_stillCamera stopCameraCapture];
}

- (void)rotateCamera
{
	[_stillCamera rotateCamera];
}

- (void)makePhoto:(void (^)(NSData *processedJPEG, NSError *error))completionHandler
{
	GPUImageFilter* emptyFilter = [GPUImageFilter new];
	[emptyFilter prepareForImageCapture];
	[self makePhoto:completionHandler withFilter:emptyFilter];
}

- (void)makePhoto:(void (^)(NSData *processedJPEG, NSError *error))completionHandler
	   withFilter:(GPUImageOutput <GPUImageInput>*)filter
{
	if (filter == nil)
	{
		filter = [GPUImageFilter new];
		[filter prepareForImageCapture];
	}
	[_stillCamera addTarget:filter];
	[_stillCamera capturePhotoAsJPEGProcessedUpToFilter:filter
								  withCompletionHandler:^(NSData *processedJPEG, NSError *error) {
									  [_stillCamera removeTarget:filter];
									  completionHandler(processedJPEG, error);
								  }];
}

- (void)setFlashMode:(AVCaptureFlashMode)flashMode
{
	AVCaptureDevice* device = self.captureDevice;
	NSError* error = nil;
	[device lockForConfiguration:&error];
	if (error != nil)
	{
		NSLog(@"_stillCamera.inputCamera lockForConfiguration error : %@", error);
		return;
	}
	[device setFlashMode:flashMode];
	[device unlockForConfiguration];
}

- (GPUImageOutput *)output
{
	return _stillCamera;
}

@end
