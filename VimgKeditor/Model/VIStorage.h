//
//  VIStorage.h
//  VimgKeditor
//
//  Created by Илья on 21.09.12.
//  Copyright (c) 2012 ilya.sergeev. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VIHandlers.h"

@interface VIStorage : NSObject

+ (NSArray*)imagesHistory;

+ (void)saveImage:(UIImage*)image
  completionBlock:(VICompletionHandler)completionBlock
	   errorBlock:(VIErrorHandler)errorBlock;

+ (void)saveToHistory:(NSURL*)imageUrl;
+ (void)removeFromHistoryImageURL:(NSURL*)imageURL;

@end
