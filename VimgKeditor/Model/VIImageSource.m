//
//  VIMaskedImage.m
//  VimgKeditor
//
//  Created by Ilya Sergeev on 01.10.12.
//  Copyright (c) 2012 ilya.sergeev@me.com. All rights reserved.
//

#import "VIImageSource.h"

#import "UIImage_resizable.h"
#import "VIDeviceInfo.h"

#define PreviewMinWidth 640
#define PreviewMinHeight 640

@implementation VIImageSource
{
	CGSize _previewSize;
}

@synthesize preview = _preview;
@synthesize gpuSource = _gpuSource;

- (id)initWithImage:(UIImage*)image
{
	return [self initWithImage:image previewSize:CGSizeMake(PreviewMinWidth, PreviewMinHeight)];
}

- (id)initWithImage:(UIImage*)image previewSize:(CGSize)previewSize
{
	self = [super init];
	if (self != nil)
	{
		_image = image;
		_previewSize = previewSize;
	}
	return self;
}

- (UIImage *)preview
{
	if (_preview == nil)
	{
		_preview = [_image imageScaledToSize:_previewSize];
	}
	return _preview;
}

- (GPUImagePicture *)gpuSource
{
	if (_gpuSource == nil)
	{
		_gpuSource = [[GPUImagePicture alloc] initWithImage:self.preview smoothlyScaleOutput:NO];
	}
	return _gpuSource;
}

@end
